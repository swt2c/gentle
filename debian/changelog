gentle (1.9+cvs20100605+dfsg1-10) UNRELEASED; urgency=medium

  * transition to wxwidgets3.2
    Closes: #1019809
  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 15 Sep 2022 09:12:11 +0200

gentle (1.9+cvs20100605+dfsg1-9) unstable; urgency=medium

  [ Steffen Moeller ]
  * debhelper-compat 13
  * d/u/metadata: Added SciCrunch RRID
  * d/control:
    - Standards-Version: 4.5.0
    - R-R-R: no

  [ Andreas Tille ]
  * Standards-Version: 4.5.1 (routine-update)
  * Add salsa-ci file (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Thu, 17 Dec 2020 15:40:47 +0100

gentle (1.9+cvs20100605+dfsg1-8) unstable; urgency=medium

  * rebuild against wxWidgets GTK 3 package
    Closes: #933449
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Remove trailing whitespace in debian/changelog
  * Drop --list-missing from dh_install

 -- Andreas Tille <tille@debian.org>  Tue, 30 Jul 2019 16:39:34 +0200

gentle (1.9+cvs20100605+dfsg1-7) unstable; urgency=medium

  [ Steffen Moeller ]
  * d/u/metadata: Added RRIDs

  [ Andreas Tille ]
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.0
  * d/rules: do not parse d/changelog
  * d/copyright: Add Files-Excluded
  * d/watch: Add comment what we could use to fetch source from Git but
    it seems that Github is lagging behind the code we have in this tarball
  * Do not install todo.txt

 -- Andreas Tille <tille@debian.org>  Fri, 24 Aug 2018 14:28:20 +0200

gentle (1.9+cvs20100605+dfsg1-6) unstable; urgency=medium

  * Fix propagation of CXXFLAGS
    Closes: #853830

 -- Andreas Tille <tille@debian.org>  Wed, 01 Feb 2017 14:23:43 +0100

gentle (1.9+cvs20100605+dfsg1-5) unstable; urgency=medium

  * cme fix dpkg-control
  * d/copyright: Source now at Github
  * add fake watch file pointing to upstream issue asking for release tags
  * Build-Depends: default-libmysqlclient-dev
    Closes: #845844
  * debhelper 10
  * d/rules: drop get-orig-source target that will not work any more but
    do not provide new one as long as upstream has not yet answered the
    issue requesting release tags
  * Remove redundant README.Debian since the information is in (and belongs
    to) README.source
  * hardening=+all
  * debian/menu: removed

 -- Andreas Tille <tille@debian.org>  Thu, 01 Dec 2016 16:02:20 +0100

gentle (1.9+cvs20100605+dfsg1-4) unstable; urgency=medium

  * Team upload
  * Add .desktop file and icons for GENtle
  * Migrate packaging to Git
  * Fix formatting of debian/copyright
  * Do not depend on libsqlite0
    - Thanks to Dmitrijs Ledkovs for the patch!
  * Refresh patches

 -- Matthias Klumpp <mak@debian.org>  Sat, 22 Aug 2015 20:30:52 +0200

gentle (1.9+cvs20100605+dfsg1-3) unstable; urgency=medium

  [ Olly Betts ]
  * Update to use wxWidgets 3.0 (new patch wx3.0-compat.patch).
  * Fix mismatching format codes in printf and scanf (new patch
    fix-format-codes.patch).
    Closes: #750906

  [ Andreas Tille ]
  * Moved debian/upstream to debian/upstream/metadata
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Wed, 25 Jun 2014 13:55:53 +0200

gentle (1.9+cvs20100605+dfsg1-2) unstable; urgency=low

  * debian/control:
     - cme fix dpkg-control
     - use anonscm in Vcs fields
     - remove unneeded Build-Depends
  * debian/rules
    - Short dh using --with autoreconf
      (Thanks to Hideki Yamane <henrich@debian.org> for his initial
       patch which gave the inspiration for the change above)
      Closes: #724221
  * debian/copyrigh: DEP5
  * install both translations of the manual but use better names and
    fix according doc-base files
  * debian/menu: Fix section
  * debian/gentle.install: Install user tips properly

 -- Andreas Tille <tille@debian.org>  Wed, 16 Oct 2013 08:19:37 +0200

gentle (1.9+cvs20100605+dfsg1-1) unstable; urgency=low

  * debian/rules:
     - Make sure that binary dlls will be deleted when unpacking
       Closes: #685149
     - Use xz compression for upstream source
     - Drop *.a files from upstream source in get-orig-source target
  * Versioned depends from DFSG free clustalw -> package can go to main
  * debian/control:
     - fix broken VCS fields
     - Standards-Version: 3.9.4 (no changes needed)
  * debian/upstream: Add citation information

 -- Andreas Tille <tille@debian.org>  Fri, 17 Aug 2012 14:36:52 +0200

gentle (1.9+cvs20100605+dfsg-3) unstable; urgency=low

  * Updated policy to 3.9.2
  * Rebuilt against later versions of mysql and tinyxml libraries

 -- Steffen Moeller <moeller@debian.org>  Fri, 03 Jun 2011 21:12:05 +0200

gentle (1.9+cvs20100605+dfsg-2) unstable; urgency=low

  * Adding build-dependency to libmysqlclient-dev (Closes: #585891)
    and to libsqlite3-dev, libsqlite0-dev
  * Changed format to 3.0 (quilt), removed dependency to cdbs and quilt,
    simplified debian/rules accordingly.
  * improved get-orig-source to remove directory checked out

 -- Steffen Moeller <moeller@debian.org>  Tue, 15 Jun 2010 16:38:33 +0200

gentle (1.9+cvs20100605+dfsg-1) unstable; urgency=low

  * Reupload in reaction to ftp-masters comments (Closes: #582959)
  * removed tinyurl and clustalw from source tree for DFSG compatibility
    - which introduced dependency to quilt
    - and provoked +dfsg suffix to version (not fully correct, yet)
  * invoking automake, autoconf during build
    - added those to build-dependencies
    - removed upstream Makefile and Makefile.in from source
  * declared 1.0 source format
  * removed doc-base lintian warning
  * explicitly listing man page as such, now correctly named GENtle.1
  * prepared for eventually adding gentle-dbg
  * compilation successfullly tested against gcc-4.5
  * added help/* to package
  * reupload to unstable

 -- Steffen Moeller <moeller@debian.org>  Wed, 02 Jun 2010 12:07:21 +0200

gentle (1.9+cvs20100531-1) experimental; urgency=low

  * Initial release.

 -- Steffen Moeller <moeller@debian.org>  Tue, 25 May 2010 00:08:31 +0200
